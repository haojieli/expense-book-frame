# 记账本

#### 介绍
记账本,日常生活记账用

#### 软件架构
主要框架：springboot 2.0
运行环境：jdk 1.8 +


#### 安装教程

1.  安装jdk 1.8 及以上
2.  安装MySQL 5.7.24 及以上
3.	创建数据库名称为：expense-book-db，基字符集选择：utf-8
3.  执行数据库脚本 resources > db > expense-book-db.sql
4.	数据库安装完成后，更改 application-dev.properties 配置文件，里面打“*”号的地方都要更改成自己的配置文件来进行调试


#### 使用说明

1.  如果想使用spring-security oauth2 的token存储在redis里面，请更改配置参数 ·security.oauth2.server.token_save_db·为redis即可
2.  另外spring-security oauth2的client配置需要修改的话，需要先修改数据库表里面的数据
3.  整套系统是基于（不完全基于，想单独使用更改登录逻辑即可）微信公众号来进行开发的，如果没有公众号，我可以提供真实的测试公众号（收取一定的费用）；也可以申请官方的测试公众号：https://mp.weixin.qq.com/debug/cgi-bin/sandbox?t=sandbox/login
4.	如对系统有任何建议可以想我反馈，后面会对系统不断的完善；微信号：wxhslhj

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

package com.hjieli.mapper;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.hjieli.entity.ExpenseBookDetail;

@Mapper
public interface ExpenseBookDetailMapper extends BaseMapper<ExpenseBookDetail> {

	@Select("select Date(create_time) as createDate from expense_book_detail where is_enable = 1 and expense_book_id = #{expenseBookId} group by Date(create_time) order by create_time desc")
    List<Map<String,Object>> dateGroups(@Param("expenseBookId") String expenseBookId);
	

	@Select("select id,type_name as typeName,money,DATE_FORMAT(create_time, #{dateFormat}) as createTime,remark,expense_book_id as expenseBookId,way "
			+ " from expense_book_detail where is_enable = 1 and expense_book_id = #{expenseBookId} and Date(create_time) = #{date} order by create_time desc ")
    List<Map<String,Object>> details(@Param("expenseBookId") String expenseBookId,@Param("date") String date,@Param("dateFormat") String dateFormat);


}

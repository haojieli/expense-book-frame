package com.hjieli.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.hjieli.entity.UserLoginInfo;

@Mapper
public interface UserLoginInfoMapper extends BaseMapper<UserLoginInfo> {

}

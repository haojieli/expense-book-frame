/*
* @author haojieli
* CreateDate 2020-12-08
* Copyright (c) 2020 haojieli
*/
package com.hjieli.controller.admin.sys;

import java.util.List;

import java.util.Map;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RestController;
import com.hjieli.entity.SysDataInfo;
import com.hjieli.service.SysDataInfoService; 
import org.springframework.web.bind.annotation.RestController;


/**
*
* @author haojieli
* @since 2020-12-08
*/
@RestController
@RequestMapping("admin/sys/sysDataInfo")
public class SysDataInfoController {

    @Autowired
    private SysDataInfoService sysDataInfoService;

    /**
     *   sys_data_info表的添加页面
    **/
	@RequestMapping("add")
	public ModelAndView add(String id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysDataInfo/view");
		if(id == null) {
			modelAndView.addObject("sysDataInfo", new SysDataInfo());
		}else {
			QueryWrapper<SysDataInfo> wrapper = new QueryWrapper<>();
			wrapper.eq("id", id);
			SysDataInfo sysDataInfo = sysDataInfoService.getOne(wrapper);
			modelAndView.addObject("sysDataInfo", sysDataInfo);
		}
		return modelAndView;
	}
    /**
     *   保存sys_data_info表的数据
    **/
    @RequestMapping(value = "save")
	public String save(@RequestBody Map<String,String> queryParams, HttpServletRequest request) {
        SysDataInfo sysDataInfo = new SysDataInfo();
            if(queryParams.containsKey("id") && !queryParams.get("id").toString().equals("")){
                sysDataInfo.setId(queryParams.get("id").toString());
            }
            if(queryParams.containsKey("dataName") && !queryParams.get("dataName").toString().equals("")){
                sysDataInfo.setDataName(queryParams.get("dataName").toString());
            }
            if(queryParams.containsKey("dataCode") && !queryParams.get("dataCode").toString().equals("")){
                sysDataInfo.setDataCode(queryParams.get("dataCode").toString());
            }
            if(queryParams.containsKey("createTime") && !queryParams.get("createTime").toString().equals("")){
            }
            if(queryParams.containsKey("updateTime") && !queryParams.get("updateTime").toString().equals("")){
            }
            if(queryParams.containsKey("isEnable") && !queryParams.get("isEnable").toString().equals("")){
                sysDataInfo.setIsEnable(Integer.parseInt(queryParams.get("isEnable")));
            }
        sysDataInfo.setIsEnable(1);
		sysDataInfo.setCreateTime(LocalDateTime.now());
		sysDataInfoService.save(sysDataInfo);
		return "success";
	}
    /**
     *   更新sys_data_info表的数据
    **/
    @RequestMapping(value = "update")
	public String update(@RequestBody SysDataInfo sysDataInfo, HttpServletRequest request) {
		
		sysDataInfoService.updateById(sysDataInfo);
		return "success";
	}


    /**
     *   删除sys_data_info表的数据
    **/
    @RequestMapping("delete")
	public String delete(@RequestBody Map<String,String> params, HttpServletRequest request) {
		
		String ids = params.get("ids").toString();
		//查询文章信息
		QueryWrapper<SysDataInfo> wrapper = new QueryWrapper<>();
		wrapper.in("id", ids.split(","));
		
	    SysDataInfo sysDataInfo = new SysDataInfo();
		sysDataInfo.setIsEnable(0);
		sysDataInfo.setCreateTime(LocalDateTime.now());
		sysDataInfoService.update(sysDataInfo,wrapper);

		return "success";
	}
    
    
    /**
     *   到sys_data_info表的列表页面
    **/
	@RequestMapping("list")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysDataInfo/list");
		return modelAndView;
	}

    /**
     *  查询sys_data_info表的数据列表
    **/
    @RequestMapping("list/data")
	public String listdata(Integer page, Integer rows,
                            String id,
                            String dataName,
                            String dataCode,
                            LocalDateTime createTime,
                            LocalDateTime updateTime,
                            Integer isEnable,
                            HttpServletRequest request) {
		
		if(page == null || page < 0) {
			page = 1;
		}
		if(rows == null || rows < 10) {
			rows = 10;
		} else if(rows > 50) {
			rows = 50;
		}
		Page<SysDataInfo> pageObj = new Page(page,rows);          //1表示当前页，而10表示每页的显示显示的条目数

        QueryWrapper<SysDataInfo> queryWrapper = new QueryWrapper();
		queryWrapper.eq(true,"is_enable", 1);
        
            if(id != null && !id.equals("")) {
                queryWrapper.eq(true,"id", id);
            }
            if(dataName != null && !dataName.equals("")) {
                queryWrapper.eq(true,"data_name", dataName);
            }
            if(dataCode != null && !dataCode.equals("")) {
                queryWrapper.eq(true,"data_code", dataCode);
            }
            if(createTime != null && !createTime.equals("")) {
                queryWrapper.eq(true,"create_time", createTime);
            }
            if(updateTime != null && !updateTime.equals("")) {
                queryWrapper.eq(true,"update_time", updateTime);
            }
            if(isEnable != null && !isEnable.equals("")) {
                queryWrapper.eq(true,"is_enable", isEnable);
            }
		queryWrapper.orderByDesc("create_time");

		
		Page<SysDataInfo> result = sysDataInfoService.listByCondition(pageObj, queryWrapper);
		Gson gson = new Gson();
		String dataStr = gson.toJson(result);
		JsonElement jsonElement = JsonParser.parseString(dataStr);
		
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		jsonObject.remove("isAsc");
		jsonObject.remove("limit");
		jsonObject.remove("offset");
		jsonObject.remove("openSort");
		jsonObject.remove("optimizeCountSql");
		jsonObject.remove("searchCount");
		jsonObject.add("rows", jsonObject.get("records").getAsJsonArray());
		jsonObject.remove("records");
		return jsonObject.toString();
	}

}


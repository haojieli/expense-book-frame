/*
* @author haojieli
* CreateDate 2020-12-08
* Copyright (c) 2020 haojieli
*/
package com.hjieli.controller.admin.sys;

import java.util.List;

import java.util.Map;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RestController;
import com.hjieli.entity.SysRoleInfo;
import com.hjieli.entity.SysRoleXMenu;
import com.hjieli.service.SysRoleInfoService;
import com.hjieli.service.SysRoleXMenuService;

import org.springframework.web.bind.annotation.RestController;


/**
*
* @author haojieli
* @since 2020-12-08
*/
@RestController
@RequestMapping("admin/sys/sysRoleInfo")
public class SysRoleInfoController {

    @Autowired
    private SysRoleInfoService sysRoleInfoService;
    
    @Autowired
    private SysRoleXMenuService sysRoleXMenuService;

    /**
     *   sys_role_info表的添加页面
    **/
	@RequestMapping("add")
	public ModelAndView add(String id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysRoleInfo/view");
		if(id == null) {
			modelAndView.addObject("sysRoleInfo", new SysRoleInfo());
		}else {
			QueryWrapper<SysRoleInfo> wrapper = new QueryWrapper<>();
			wrapper.eq("id", id);
			SysRoleInfo sysRoleInfo = sysRoleInfoService.getOne(wrapper);
			modelAndView.addObject("sysRoleInfo", sysRoleInfo);
		}
		return modelAndView;
	}
    /**
     *   保存sys_role_info表的数据
    **/
    @RequestMapping(value = "save")
	public String save(@RequestBody Map<String,String> queryParams, HttpServletRequest request) {
        SysRoleInfo sysRoleInfo = new SysRoleInfo();
            if(queryParams.containsKey("id") && !queryParams.get("id").toString().equals("")){
                sysRoleInfo.setId(queryParams.get("id").toString());
            }
            if(queryParams.containsKey("roleName") && !queryParams.get("roleName").toString().equals("")){
                sysRoleInfo.setRoleName(queryParams.get("roleName").toString());
            }
            if(queryParams.containsKey("roleTag") && !queryParams.get("roleTag").toString().equals("")){
                sysRoleInfo.setRoleTag(queryParams.get("roleTag").toString());
            }
            if(queryParams.containsKey("createTime") && !queryParams.get("createTime").toString().equals("")){
            }
            if(queryParams.containsKey("updateTime") && !queryParams.get("updateTime").toString().equals("")){
            }
            if(queryParams.containsKey("isEnable") && !queryParams.get("isEnable").toString().equals("")){
                sysRoleInfo.setIsEnable(Integer.parseInt(queryParams.get("isEnable")));
            }
        sysRoleInfo.setIsEnable(1);
		sysRoleInfo.setCreateTime(LocalDateTime.now());
		sysRoleInfoService.save(sysRoleInfo);
		return "success";
	}
    /**
     *   更新sys_role_info表的数据
    **/
    @RequestMapping(value = "update")
	public String update(@RequestBody SysRoleInfo sysRoleInfo, HttpServletRequest request) {
    	sysRoleInfo.setUpdateTime(LocalDateTime.now());
		sysRoleInfoService.updateById(sysRoleInfo);
		return "success";
	}


    /**
     *   删除sys_role_info表的数据
    **/
    @RequestMapping("delete")
	public String delete(@RequestBody Map<String,String> params, HttpServletRequest request) {
		
		String ids = params.get("ids").toString();
		//查询文章信息
		QueryWrapper<SysRoleInfo> wrapper = new QueryWrapper<>();
		wrapper.in("id", ids.split(","));
		
	    SysRoleInfo sysRoleInfo = new SysRoleInfo();
		sysRoleInfo.setIsEnable(0);
		sysRoleInfo.setCreateTime(LocalDateTime.now());
		sysRoleInfoService.update(sysRoleInfo,wrapper);

		return "success";
	}
    
    
    /**
     *   到sys_role_info表的列表页面
    **/
	@RequestMapping("list")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysRoleInfo/list");
		return modelAndView;
	}

    /**
     *  查询sys_role_info表的数据列表
    **/
    @RequestMapping("list/data")
	public String listdata(Integer page, Integer rows,
                            String id,
                            String roleName,
                            String roleTag,
                            LocalDateTime createTime,
                            LocalDateTime updateTime,
                            Integer isEnable,
                            HttpServletRequest request) {
		
		if(page == null || page < 0) {
			page = 1;
		}
		if(rows == null || rows < 10) {
			rows = 10;
		} else if(rows > 50) {
			rows = 50;
		}
		Page<SysRoleInfo> pageObj = new Page(page,rows);          //1表示当前页，而10表示每页的显示显示的条目数

        QueryWrapper<SysRoleInfo> queryWrapper = new QueryWrapper();
		queryWrapper.eq(true,"is_enable", 1);
        
            if(id != null && !id.equals("")) {
                queryWrapper.eq(true,"id", id);
            }
            if(roleName != null && !roleName.equals("")) {
                queryWrapper.eq(true,"role_name", roleName);
            }
            if(roleTag != null && !roleTag.equals("")) {
                queryWrapper.eq(true,"role_tag", roleTag);
            }
            if(createTime != null && !createTime.equals("")) {
                queryWrapper.eq(true,"create_time", createTime);
            }
            if(updateTime != null && !updateTime.equals("")) {
                queryWrapper.eq(true,"update_time", updateTime);
            }
            if(isEnable != null && !isEnable.equals("")) {
                queryWrapper.eq(true,"is_enable", isEnable);
            }
		queryWrapper.orderByDesc("create_time");

		
		Page<SysRoleInfo> result = sysRoleInfoService.listByCondition(pageObj, queryWrapper);
		Gson gson = new Gson();
		String dataStr = gson.toJson(result);
		JsonElement jsonElement = JsonParser.parseString(dataStr);
		
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		jsonObject.remove("isAsc");
		jsonObject.remove("limit");
		jsonObject.remove("offset");
		jsonObject.remove("openSort");
		jsonObject.remove("optimizeCountSql");
		jsonObject.remove("searchCount");
		jsonObject.add("rows", jsonObject.get("records").getAsJsonArray());
		jsonObject.remove("records");
		return jsonObject.toString();
	}
    
    /**
     *   到sys_role_info表的列表页面
    **/
	@RequestMapping("rolexmenu")
	public ModelAndView roleXMenu(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysRoleInfo/roleXMenuView");
		String roleId = request.getParameter("roleId");
		QueryWrapper<SysRoleInfo> queryWrapper = new QueryWrapper<SysRoleInfo>();
		queryWrapper.eq("id", roleId);
		SysRoleInfo sysRoleInfo = sysRoleInfoService.getOne(queryWrapper);
		request.setAttribute("sysRoleInfo",sysRoleInfo);
		
		//获取该角色已拥有的权限
		QueryWrapper<SysRoleXMenu> queryWrappers = new QueryWrapper<SysRoleXMenu>();
		queryWrappers.eq("role_id", roleId);
		List<SysRoleXMenu> sysRoleXMenuList = sysRoleXMenuService.list(queryWrappers);
		String menuIds = "";
		for(int index = 0 ; index < sysRoleXMenuList.size(); index ++) {
			if(index != 0) {
				menuIds += ",";
			}
			menuIds += sysRoleXMenuList.get(index).getMenuId();
		}
		request.setAttribute("menuIds",menuIds);
		
		return modelAndView;
	}

}


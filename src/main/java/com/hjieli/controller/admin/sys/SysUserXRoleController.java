/*
* @author haojieli
* CreateDate 2020-12-08
* Copyright (c) 2020 haojieli
*/
package com.hjieli.controller.admin.sys;

import java.util.List;

import java.util.Map;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RestController;
import com.hjieli.entity.SysUserXRole;
import com.hjieli.service.SysUserXRoleService; 
import org.springframework.web.bind.annotation.RestController;


/**
*
* @author haojieli
* @since 2020-12-08
*/
@RestController
@RequestMapping("admin/sys/sysUserXRole")
public class SysUserXRoleController {

    @Autowired
    private SysUserXRoleService sysUserXRoleService;

    /**
     *   sys_user_x_role表的添加页面
    **/
	@RequestMapping("add")
	public ModelAndView add(String id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysUserXRole/view");
		if(id == null) {
			modelAndView.addObject("sysUserXRole", new SysUserXRole());
		}else {
			QueryWrapper<SysUserXRole> wrapper = new QueryWrapper<>();
			wrapper.eq("id", id);
			SysUserXRole sysUserXRole = sysUserXRoleService.getOne(wrapper);
			modelAndView.addObject("sysUserXRole", sysUserXRole);
		}
		return modelAndView;
	}
    /**
     *   保存sys_user_x_role表的数据
    **/
    @RequestMapping(value = "save")
	public String save(@RequestBody Map<String,String> queryParams, HttpServletRequest request) {
        SysUserXRole sysUserXRole = new SysUserXRole();
            if(queryParams.containsKey("id") && !queryParams.get("id").toString().equals("")){
                sysUserXRole.setId(queryParams.get("id").toString());
            }
            if(queryParams.containsKey("userId") && !queryParams.get("userId").toString().equals("")){
                sysUserXRole.setUserId(queryParams.get("userId").toString());
            }
            if(queryParams.containsKey("roleId") && !queryParams.get("roleId").toString().equals("")){
                sysUserXRole.setRoleId(queryParams.get("roleId").toString());
            }
            if(queryParams.containsKey("createTime") && !queryParams.get("createTime").toString().equals("")){
            }
            if(queryParams.containsKey("updateTime") && !queryParams.get("updateTime").toString().equals("")){
            }
            if(queryParams.containsKey("isEnable") && !queryParams.get("isEnable").toString().equals("")){
                sysUserXRole.setIsEnable(Integer.parseInt(queryParams.get("isEnable")));
            }
        sysUserXRole.setIsEnable(1);
		sysUserXRole.setCreateTime(LocalDateTime.now());
		sysUserXRoleService.save(sysUserXRole);
		return "success";
	}
    /**
     *   更新sys_user_x_role表的数据
    **/
    @RequestMapping(value = "update")
	public String update(@RequestBody SysUserXRole sysUserXRole, HttpServletRequest request) {
		
		sysUserXRoleService.updateById(sysUserXRole);
		return "success";
	}


    /**
     *   删除sys_user_x_role表的数据
    **/
    @RequestMapping("delete")
	public String delete(@RequestBody Map<String,String> params, HttpServletRequest request) {
		
		String ids = params.get("ids").toString();
		//查询文章信息
		QueryWrapper<SysUserXRole> wrapper = new QueryWrapper<>();
		wrapper.in("id", ids.split(","));
		
	    SysUserXRole sysUserXRole = new SysUserXRole();
		sysUserXRole.setIsEnable(0);
		sysUserXRole.setCreateTime(LocalDateTime.now());
		sysUserXRoleService.update(sysUserXRole,wrapper);

		return "success";
	}
    
    
    /**
     *   到sys_user_x_role表的列表页面
    **/
	@RequestMapping("list")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysUserXRole/list");
		return modelAndView;
	}

    /**
     *  查询sys_user_x_role表的数据列表
    **/
    @RequestMapping("list/data")
	public String listdata(Integer page, Integer rows,
                            String id,
                            String userId,
                            String roleId,
                            LocalDateTime createTime,
                            LocalDateTime updateTime,
                            Integer isEnable,
                            HttpServletRequest request) {
		
		if(page == null || page < 0) {
			page = 1;
		}
		if(rows == null || rows < 10) {
			rows = 10;
		} else if(rows > 50) {
			rows = 50;
		}
		Page<SysUserXRole> pageObj = new Page(page,rows);          //1表示当前页，而10表示每页的显示显示的条目数

        QueryWrapper<SysUserXRole> queryWrapper = new QueryWrapper();
		queryWrapper.eq(true,"is_enable", 1);
        
            if(id != null && !id.equals("")) {
                queryWrapper.eq(true,"id", id);
            }
            if(userId != null && !userId.equals("")) {
                queryWrapper.eq(true,"user_id", userId);
            }
            if(roleId != null && !roleId.equals("")) {
                queryWrapper.eq(true,"role_id", roleId);
            }
            if(createTime != null && !createTime.equals("")) {
                queryWrapper.eq(true,"create_time", createTime);
            }
            if(updateTime != null && !updateTime.equals("")) {
                queryWrapper.eq(true,"update_time", updateTime);
            }
            if(isEnable != null && !isEnable.equals("")) {
                queryWrapper.eq(true,"is_enable", isEnable);
            }
		queryWrapper.orderByDesc("create_time");

		
		Page<SysUserXRole> result = sysUserXRoleService.listByCondition(pageObj, queryWrapper);
		Gson gson = new Gson();
		String dataStr = gson.toJson(result);
		JsonElement jsonElement = JsonParser.parseString(dataStr);
		
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		jsonObject.remove("isAsc");
		jsonObject.remove("limit");
		jsonObject.remove("offset");
		jsonObject.remove("openSort");
		jsonObject.remove("optimizeCountSql");
		jsonObject.remove("searchCount");
		jsonObject.add("rows", jsonObject.get("records").getAsJsonArray());
		jsonObject.remove("records");
		return jsonObject.toString();
	}

}


/*
* @author haojieli
* CreateDate 2020-12-30
* Copyright (c) 2020 haojieli
*/
package com.hjieli.controller.api;

import java.util.List;

import java.util.Map;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RestController;
import com.hjieli.entity.WeixinUserInfo;
import com.hjieli.service.WeixinUserInfoService; 
import org.springframework.web.bind.annotation.RestController;


/**
*
* @author haojieli
* @since 2020-12-30
*/
@RestController
@RequestMapping("admin/sys/weixinUserInfo")
public class WeixinUserInfoController {

    @Autowired
    private WeixinUserInfoService weixinUserInfoService;

    /**
     *   weixin_user_info表的添加页面
    **/
	@RequestMapping("add")
	public ModelAndView add(String id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/weixinUserInfo/view");
		if(id == null) {
			modelAndView.addObject("weixinUserInfo", new WeixinUserInfo());
		}else {
			QueryWrapper<WeixinUserInfo> wrapper = new QueryWrapper<>();
			wrapper.eq("id", id);
			WeixinUserInfo weixinUserInfo = weixinUserInfoService.getOne(wrapper);
			modelAndView.addObject("weixinUserInfo", weixinUserInfo);
		}
		return modelAndView;
	}
    /**
     *   保存weixin_user_info表的数据
    **/
    @RequestMapping(value = "save")
	public String save(@RequestBody Map<String,String> queryParams, HttpServletRequest request) {
        WeixinUserInfo weixinUserInfo = new WeixinUserInfo();
            if(queryParams.containsKey("id") && !queryParams.get("id").toString().equals("")){
                weixinUserInfo.setId(queryParams.get("id").toString());
            }
            if(queryParams.containsKey("userId") && !queryParams.get("userId").toString().equals("")){
                weixinUserInfo.setUserId(queryParams.get("userId").toString());
            }
            if(queryParams.containsKey("openId") && !queryParams.get("openId").toString().equals("")){
                weixinUserInfo.setOpenId(queryParams.get("openId").toString());
            }
            if(queryParams.containsKey("nickname") && !queryParams.get("nickname").toString().equals("")){
                weixinUserInfo.setNickname(queryParams.get("nickname").toString());
            }
            if(queryParams.containsKey("sex") && !queryParams.get("sex").toString().equals("")){
                weixinUserInfo.setSex(queryParams.get("sex").toString());
            }
            if(queryParams.containsKey("language") && !queryParams.get("language").toString().equals("")){
                weixinUserInfo.setLanguage(queryParams.get("language").toString());
            }
            if(queryParams.containsKey("city") && !queryParams.get("city").toString().equals("")){
                weixinUserInfo.setCity(queryParams.get("city").toString());
            }
            if(queryParams.containsKey("province") && !queryParams.get("province").toString().equals("")){
                weixinUserInfo.setProvince(queryParams.get("province").toString());
            }
            if(queryParams.containsKey("country") && !queryParams.get("country").toString().equals("")){
                weixinUserInfo.setCountry(queryParams.get("country").toString());
            }
            if(queryParams.containsKey("headImgUrl") && !queryParams.get("headImgUrl").toString().equals("")){
                weixinUserInfo.setHeadImgUrl(queryParams.get("headImgUrl").toString());
            }
            if(queryParams.containsKey("isEnable") && !queryParams.get("isEnable").toString().equals("")){
                weixinUserInfo.setIsEnable(Integer.parseInt(queryParams.get("isEnable")));
            }
            if(queryParams.containsKey("createTime") && !queryParams.get("createTime").toString().equals("")){
            }
            if(queryParams.containsKey("updateTime") && !queryParams.get("updateTime").toString().equals("")){
            }
        weixinUserInfo.setIsEnable(1);
		weixinUserInfo.setCreateTime(LocalDateTime.now());
		weixinUserInfoService.save(weixinUserInfo);
		return "success";
	}
    /**
     *   更新weixin_user_info表的数据
    **/
    @RequestMapping(value = "update")
	public String update(@RequestBody WeixinUserInfo weixinUserInfo, HttpServletRequest request) {
		
		weixinUserInfoService.updateById(weixinUserInfo);
		return "success";
	}


    /**
     *   删除weixin_user_info表的数据
    **/
    @RequestMapping("delete")
	public String delete(@RequestBody Map<String,String> params, HttpServletRequest request) {
		
		String ids = params.get("ids").toString();
		//查询文章信息
		QueryWrapper<WeixinUserInfo> wrapper = new QueryWrapper<>();
		wrapper.in("id", ids.split(","));
		
	    WeixinUserInfo weixinUserInfo = new WeixinUserInfo();
		weixinUserInfo.setIsEnable(0);
		weixinUserInfo.setCreateTime(LocalDateTime.now());
		weixinUserInfoService.update(weixinUserInfo,wrapper);

		return "success";
	}
    
    
    /**
     *   到weixin_user_info表的列表页面
    **/
	@RequestMapping("list")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/weixinUserInfo/list");
		return modelAndView;
	}

    /**
     *  查询weixin_user_info表的数据列表
    **/
    @RequestMapping("list/data")
	public String listdata(Integer page, Integer rows,
                            String id,
                            String userId,
                            String openId,
                            String nickname,
                            String sex,
                            String language,
                            String city,
                            String province,
                            String country,
                            String headImgUrl,
                            Integer isEnable,
                            LocalDateTime createTime,
                            LocalDateTime updateTime,
                            HttpServletRequest request) {
		
		if(page == null || page < 0) {
			page = 1;
		}
		if(rows == null || rows < 10) {
			rows = 10;
		} else if(rows > 50) {
			rows = 50;
		}
		Page<WeixinUserInfo> pageObj = new Page(page,rows);          //1表示当前页，而10表示每页的显示显示的条目数

        QueryWrapper<WeixinUserInfo> queryWrapper = new QueryWrapper();
		queryWrapper.eq(true,"is_enable", 1);
        
            if(id != null && !id.equals("")) {
                queryWrapper.eq(true,"id", id);
            }
            if(userId != null && !userId.equals("")) {
                queryWrapper.eq(true,"user_id", userId);
            }
            if(openId != null && !openId.equals("")) {
                queryWrapper.eq(true,"openId", openId);
            }
            if(nickname != null && !nickname.equals("")) {
                queryWrapper.eq(true,"nickname", nickname);
            }
            if(sex != null && !sex.equals("")) {
                queryWrapper.eq(true,"sex", sex);
            }
            if(language != null && !language.equals("")) {
                queryWrapper.eq(true,"language", language);
            }
            if(city != null && !city.equals("")) {
                queryWrapper.eq(true,"city", city);
            }
            if(province != null && !province.equals("")) {
                queryWrapper.eq(true,"province", province);
            }
            if(country != null && !country.equals("")) {
                queryWrapper.eq(true,"country", country);
            }
            if(headImgUrl != null && !headImgUrl.equals("")) {
                queryWrapper.eq(true,"head_img_url", headImgUrl);
            }
            if(isEnable != null && !isEnable.equals("")) {
                queryWrapper.eq(true,"is_enable", isEnable);
            }
            if(createTime != null && !createTime.equals("")) {
                queryWrapper.eq(true,"create_time", createTime);
            }
            if(updateTime != null && !updateTime.equals("")) {
                queryWrapper.eq(true,"update_time", updateTime);
            }
		queryWrapper.orderByDesc("create_time");

		
		Page<WeixinUserInfo> result = weixinUserInfoService.listByCondition(pageObj, queryWrapper);
		Gson gson = new Gson();
		String dataStr = gson.toJson(result);
		JsonElement jsonElement = JsonParser.parseString(dataStr);
		
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		jsonObject.remove("isAsc");
		jsonObject.remove("limit");
		jsonObject.remove("offset");
		jsonObject.remove("openSort");
		jsonObject.remove("optimizeCountSql");
		jsonObject.remove("searchCount");
		jsonObject.add("rows", jsonObject.get("records").getAsJsonArray());
		jsonObject.remove("records");
		return jsonObject.toString();
	}

}


package com.hjieli.config.security.oauth;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import com.hjieli.config.security.util.MD5PasswordEncoder;

@Configuration
@EnableAuthorizationServer
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter{

	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	RedisConnectionFactory redisConnectionFactory;
	
	@Autowired
	UserDetailsService userDetailsService;
	
	@Resource
    private DataSource dataSource;
	
	@Autowired
    private PasswordEncoder passwordEncoder;
	
//	@Autowired
//	private MD5PasswordEncoder mD5PasswordEncoder;

	@Value("${security.oauth2.server.token_save_db}")
	private String tokenSaveDb;
	
	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		// TODO Auto-generated method stub
		security.allowFormAuthenticationForClients();
		
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		// TODO Auto-generated method stub
		clients.withClientDetails(clientDetails());
		
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		// TODO Auto-generated method stub
		
		
		if(tokenSaveDb.equals("MySQL")) {
			endpoints.tokenStore(new JdbcTokenStore(dataSource));
		}else {
			endpoints.tokenStore(new RedisTokenStore(redisConnectionFactory));
		}
		endpoints.authenticationManager(authenticationManager)
			.userDetailsService(userDetailsService);
	}
	
	@Bean
    public ClientDetailsService clientDetails() {
		JdbcClientDetailsService jdbcClientDetailsService = new JdbcClientDetailsService(dataSource);
		jdbcClientDetailsService.setPasswordEncoder(passwordEncoder);
		
        return jdbcClientDetailsService;
    }
	
	
}

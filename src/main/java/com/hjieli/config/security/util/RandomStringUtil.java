package com.hjieli.config.security.util;

import java.text.NumberFormat;
import java.util.Random;

public class RandomStringUtil {

	public static String randomNum() {
		
		return randomNum(6);
	}
	
	public static String randomNum(int length) {
		
		String [] nums = "1,2,3,4,5,6,7,8,9".split(",");
		String temp = "";
		int tempIndex = 0;
		for(int index = 0; index < length; index ++) {
			tempIndex = new Random().nextInt(8);
			temp += nums[tempIndex];
		}
		return temp;
	}
	
	public static String randomVerifyNum() {
		return randomVerifyNum(6);
	}
	
	public static String randomVerifyNum(int length) {
		
		String [] nums = "1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,J,K,M,N,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,j,k,m,n,p,q,r,s,t,u,v,w,x,y,z".split(",");
		String temp = "";
		int tempIndex = 0;
		for(int index = 0; index < length; index ++) {
			tempIndex = new Random().nextInt(9) - 1;
			temp += nums[tempIndex];
		}
		return temp;
	}
	
	public static String randomFormatNum(int length,int numLength) {
		String tempStr = "";
		if(numLength > 9) {
			numLength = 9;
		}
		if(numLength <= 0) {
			numLength = 1;
		}
		for(int index = 0; index < numLength; index ++) {
			tempStr += "9";
		}
		int endNum = Integer.parseInt(tempStr);
		String result = new Random().nextInt(endNum) + "";
		
		return randomFormatNum(length,result);
	}

	public static String randomFormatNum(int length,String resultNum) {
		NumberFormat formatter = NumberFormat.getNumberInstance();   
        formatter.setMinimumIntegerDigits(length); 
        formatter.setGroupingUsed(false);
        resultNum = formatter.format(Integer.parseInt(resultNum));
		return resultNum;
	}

	public static String randomFormatNum(String prefix, int length, int numLength) {
		prefix = prefix + randomFormatNum(length,numLength);
		return prefix;
	}

	public static String randomFormatNum(String prefix, int length, String resultNum) {
		prefix = prefix + randomFormatNum(length,resultNum);
		return prefix;
	}
//	public static void main(String[] args) {
//		for(int index = 0; index < 10; index ++) {
//			System.out.println(randomFormatNum(9,5));
//			System.out.println(randomFormatNum("NO.",8,5));
//		}
//	}
}

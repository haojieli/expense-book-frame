package com.hjieli.config.interceptor;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;


public class ErrorInterceptor implements HandlerInterceptor {
	private static Logger logger = LoggerFactory.getLogger(ErrorInterceptor.class);
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		String requestUrl = request.getRequestURI();
		if(requestUrl.contains("/admin/")) {
			Principal principal = request.getUserPrincipal();
			if(principal == null) {
//				request.getRequestDispatcher("/login").forward(request, response);
				response.sendRedirect("/login");
				return false;
			}
			
		}
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		//logger.debug(ex.toString());
		HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
		
	}

	
}

package com.hjieli.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author haojieli
 * @since 2021-01-04
 */
public class ExpenseBookInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    private String title;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private String nowMonthPayOut;

    private String nowMonthPutIn;

    private Integer isEnable;

    private String createUserId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public String getNowMonthPayOut() {
        return nowMonthPayOut;
    }

    public void setNowMonthPayOut(String nowMonthPayOut) {
        this.nowMonthPayOut = nowMonthPayOut;
    }
    public String getNowMonthPutIn() {
        return nowMonthPutIn;
    }

    public void setNowMonthPutIn(String nowMonthPutIn) {
        this.nowMonthPutIn = nowMonthPutIn;
    }
    public Integer getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Integer isEnable) {
        this.isEnable = isEnable;
    }
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    @Override
    public String toString() {
        return "ExpenseBookInfo{" +
            "id=" + id +
            ", title=" + title +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", nowMonthPayOut=" + nowMonthPayOut +
            ", nowMonthPutIn=" + nowMonthPutIn +
            ", isEnable=" + isEnable +
            ", createUserId=" + createUserId +
        "}";
    }
}

package com.hjieli.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjieli.mapper.SysUserInfoMapper;
import com.hjieli.entity.SysUserInfo;

@Service("sysUserInfoService")
public class SysUserInfoService extends ServiceImpl<SysUserInfoMapper, SysUserInfo> {
     
	public Page<SysUserInfo> listByCondition(Page<SysUserInfo> page, QueryWrapper<SysUserInfo> queryWrapper) {
		return  baseMapper.selectPage(page, queryWrapper);
	}
	
	
}

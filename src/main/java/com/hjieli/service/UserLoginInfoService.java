package com.hjieli.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjieli.mapper.UserLoginInfoMapper;
import com.hjieli.entity.UserLoginInfo;

@Service("userLoginInfoService")
public class UserLoginInfoService extends ServiceImpl<UserLoginInfoMapper, UserLoginInfo> {
     
	public Page<UserLoginInfo> listByCondition(Page<UserLoginInfo> page, QueryWrapper<UserLoginInfo> queryWrapper) {
		return  baseMapper.selectPage(page, queryWrapper);
	}
	
	
}

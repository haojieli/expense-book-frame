package com.hjieli.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjieli.mapper.SysMenuInfoMapper;
import com.hjieli.entity.SysMenuInfo;

@Service("sysMenuInfoService")
public class SysMenuInfoService extends ServiceImpl<SysMenuInfoMapper, SysMenuInfo> {
     
	public Page<SysMenuInfo> listByCondition(Page<SysMenuInfo> page, QueryWrapper<SysMenuInfo> queryWrapper) {
		return  baseMapper.selectPage(page, queryWrapper);
	}
	
	
}

package com.hjieli.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjieli.mapper.ExpenseBookDetailMapper;
import com.hjieli.entity.ExpenseBookDetail;

@Service("expenseBookDetailService")
public class ExpenseBookDetailService extends ServiceImpl<ExpenseBookDetailMapper, ExpenseBookDetail> {
     
	public Page<ExpenseBookDetail> listByCondition(Page<ExpenseBookDetail> page, QueryWrapper<ExpenseBookDetail> queryWrapper) {
		return  baseMapper.selectPage(page, queryWrapper);
	}
	
	public List<Map<String,Object>> dateGroups(String expenseBookId){
		return baseMapper.dateGroups(expenseBookId);
	};
	

	public List<Map<String,Object>> details(String expenseBookId,String date,String dateFormat){
		return baseMapper.details(expenseBookId, date,dateFormat);
	}

}

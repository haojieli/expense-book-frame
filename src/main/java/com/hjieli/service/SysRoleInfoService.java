package com.hjieli.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjieli.mapper.SysRoleInfoMapper;
import com.hjieli.entity.SysRoleInfo;

@Service("sysRoleInfoService")
public class SysRoleInfoService extends ServiceImpl<SysRoleInfoMapper, SysRoleInfo> {
     
	public Page<SysRoleInfo> listByCondition(Page<SysRoleInfo> page, QueryWrapper<SysRoleInfo> queryWrapper) {
		return  baseMapper.selectPage(page, queryWrapper);
	}
	
	
}

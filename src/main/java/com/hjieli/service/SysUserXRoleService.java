package com.hjieli.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjieli.mapper.SysUserXRoleMapper;
import com.hjieli.entity.SysUserXRole;

@Service("sysUserXRoleService")
public class SysUserXRoleService extends ServiceImpl<SysUserXRoleMapper, SysUserXRole> {
     
	public Page<SysUserXRole> listByCondition(Page<SysUserXRole> page, QueryWrapper<SysUserXRole> queryWrapper) {
		return  baseMapper.selectPage(page, queryWrapper);
	}
	
	
}

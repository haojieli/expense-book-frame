package com.hjieli.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjieli.mapper.WeixinUserInfoMapper;
import com.hjieli.entity.WeixinUserInfo;

@Service("weixinUserInfoService")
public class WeixinUserInfoService extends ServiceImpl<WeixinUserInfoMapper, WeixinUserInfo> {
     
	public Page<WeixinUserInfo> listByCondition(Page<WeixinUserInfo> page, QueryWrapper<WeixinUserInfo> queryWrapper) {
		return  baseMapper.selectPage(page, queryWrapper);
	}
	
	
}

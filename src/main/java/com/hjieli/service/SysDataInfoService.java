package com.hjieli.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjieli.mapper.SysDataInfoMapper;
import com.hjieli.entity.SysDataInfo;

@Service("sysDataInfoService")
public class SysDataInfoService extends ServiceImpl<SysDataInfoMapper, SysDataInfo> {
     
	public Page<SysDataInfo> listByCondition(Page<SysDataInfo> page, QueryWrapper<SysDataInfo> queryWrapper) {
		return  baseMapper.selectPage(page, queryWrapper);
	}
	
	
}

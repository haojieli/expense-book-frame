package com.hjieli;

import com.hjieli.config.security.util.MD5PasswordEncoder;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("com.hjieli.mapper")
@EnableTransactionManagement(proxyTargetClass = true)
public class ExpenseBookApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExpenseBookApplication.class, args);
//		System.out.println(new MD5PasswordEncoder().encode("admin"));
	}

}
